Source: djoser
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-babel,
 python3-django,
 python3-djangorestframework (>= 3),
 python3-pypandoc,
 python3-setuptools,
Standards-Version: 4.5.1
Homepage: https://github.com/sunscrapers/djoser
Vcs-Browser: https://salsa.debian.org/python-team/packages/djoser
Vcs-Git: https://salsa.debian.org/python-team/packages/djoser.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-djoser
Architecture: all
Depends:
 python3-django,
 python3-djangorestframework (>= 3),
 ${misc:Depends},
 ${python3:Depends},
Description: REST implementation of Django authentication system (Python3 version)
 Djoser library provides a set of Django Rest Framework views to handle basic
 actions such as registration, login, logout, password reset and account
 activation. It works with custom user model.
 .
 Instead of reusing Django code (e.g. PasswordResetForm), it reimplementes a few
 things to fit better into a Single Page App architecture.
 .
 Supported authentication backends are:
  * HTTP Basic Auth (Default)
  * Token based authentication from Django Rest Framework
 .
 This package contains the Python 3 version of the library.
